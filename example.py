#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import sys

from convenience import xopen
from convenience import Logger
from convenience import header, blue, green, yellow, orange, red, bold, underline

if __name__=="__main__":
    desc = "Demo for Convenience module. Try calling me on gzipped or uncompressed files and changing verbosity (-v/-vv/-vvv)."
    parser = argparse.ArgumentParser(description=desc, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("--input", "-i", default=sys.argv[0], help="Input file (default: {})".format(sys.argv[0]))
    parser.add_argument("-v", "--verbosity", action="count", default=0, help="increase verbosity")
    args = parser.parse_args()
    logger = Logger(args.verbosity)
    
    logger.info("Printing colors...")
    
    print(header("I'm a born header!"))
    print(blue("Printing something out of the blue."))
    print(green("Go green!"))
    print(yellow("Mellow yellow!"))
    print(orange("That's some clockwork orange."))
    print(red("Better red than dead."))
    print(bold("That's some bold statement."))
    print(underline("Stop underlining my authority!"))
    print("Or "+header("all "+blue("at the "+bold("same "+underline("time.")))))
    
    logger.info("Reading file...")
    if args.input == sys.argv[0]:
        logger.info(red("Printin' a me self, ye!"))
    try:
        with xopen(args.input) as f:
            for l in f:
                print(l[:-1])
    except:
        logger.debug("Something went wrong while reading {}".format(args.input))
        logger.error("Could not read input file!")
