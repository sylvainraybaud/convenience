name = "convenience"

from .convenience import xopen
from .convenience import Logger
from .convenience import header, blue, green, yellow, orange, red, bold, underline

__all__ = [
    "xopen",
    "Logger",
    "header",
    "blue",
    "green",
    "yellow",
    "orange",
    "red",
    "bold",
    "underline"
]
