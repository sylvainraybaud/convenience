# Convenience
Tools for everyday use.

Source and install files are available under BSD License here: [https://gitlab.com/sylvainraybaud/convenience](https://gitlab.com/sylvainraybaud/convenience)

This package *HAS NOT* been tested with Python 2.

## API

* ``xopen``: a wrapper around ``open`` and ``gzip.open`` returning an ``XFile`` object, which is a wrapper around ``File`` and ``gzip.GzipFile``. ``XFile`` implements methods ``open``, ``close``, ``read``, ``readline``, ``readlines``, ``write`` with the same parameters as ``File`` methods. It also implements ``__iter__`` and ``__next__`` to make it iterable and ``__enter__`` and ``__exit__`` to make it usable in ``with`` statements. Files whose names end with *.gz* are treated as gzip files, all other files are treated as uncompressed files.

* ``Logger``: a wrapper around ``logging`` module. A ``Logger`` object is intialized with a ``verbosity`` parameter which is translated into a ``logging`` value (``WARNING``, ``INFO`` or ``DEBUG``). It implements ```debug```, ```info```, ```warning``` and ``error`` methods. They all take a string as first argument argument. ``error`` takes an extra argument, ``code``. When called, ``error`` print its first parameter then calls ``exit(code)``.

* ```header, blue, green, yellow, orange, red, bold, underline```: apply decoration to string (see ```example.py```)

## Using

A good example being worth a thousand words, please see [example.py](https://gitlab.com/sylvainraybaud/convenience/blob/master/example.py)

## Installing

````bash
pip install convenience
````

## TODO

* Use magic number instead of file extension to detected gzipped files, when applicable.
* Handle more compression format.
* Double check handling of various text encodings.
* Add more magic.

## Contributing

Please make a pull request on gitlab. You can mail me at 

````first_name [dot] family_name [at] crans [dot] org````
